$(document).ready(function(){
	$('a[href^="#"]').on('click',function (e) {
	    e.preventDefault();

	    var target = this.hash;
	    var $target = $(target);

	    $('html, body').stop().animate({
	        'scrollTop': $target.offset().top
	    }, 900, 'swing', function () {
	        window.location.hash = target;
	    });
	});
  
  //Project hover animation 
  $('#portfolio .thumbnail').hover(function(){
    $(this).children(".cust-caption").slideDown();
  },
  function(){
    $(this).children(".cust-caption").fadeOut();
  });
  
  	$('#contactPageForm').validate({
        
        rules : {
            "name" : {required : true, minlength: 3},
            "email" : {required : true, email : true},
            "message" : { required : true, minlength: 10}
        },
        
        messages : {
            "name" : "Przedstaw się.",
            "email" : "Wprowadź poprawny email",
            "message" : "Przydałaby się jakaś treść"
        }
        
    });
  
});
  
 //Skills' animation
$(window).scroll(function(){
  jQuery('.skillbar').each(function(){
			jQuery(this).find('.skillbar-bar').animate({
				width:jQuery(this).attr('data-percent')
			},6000);
		});
});